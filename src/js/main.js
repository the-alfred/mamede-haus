/*  ---------------------------------------------------
    Template Name: Ogani
    Description:  Ogani eCommerce  HTML Template
    Author: Colorlib
    Author URI: https://colorlib.com
    Version: 1.0
    Created: Colorlib
---------------------------------------------------------  */

'use strict';

(function($) {

    /*------------------
        Preloader
    --------------------*/
    $(window).on('load', function() {
        $(".loader").fadeOut();
        $("#preloder").delay(200).fadeOut("slow");

        /*------------------
            Gallery filter
        --------------------*/
        $('.featured__controls li').on('click', function() {
            $('.featured__controls li').removeClass('active');
            $(this).addClass('active');
        });
        if ($('.featured__filter').length > 0) {
            var containerEl = document.querySelector('.featured__filter');
            var mixer = mixitup(containerEl);
        }
    });

    /*------------------
        Background Set
    --------------------*/
    $('.set-bg').each(function() {
        var bg = $(this).data('setbg');
        $(this).css('background-image', 'url(' + bg + ')');
    });

    //Humberger Menu
    $(".humberger__open").on('click', function() {
        $(".humberger__menu__wrapper").addClass("show__humberger__menu__wrapper");
        $(".humberger__menu__overlay").addClass("active");
        $("body").addClass("over_hid");
    });

    $(".humberger__menu__overlay").on('click', function() {
        $(".humberger__menu__wrapper").removeClass("show__humberger__menu__wrapper");
        $(".humberger__menu__overlay").removeClass("active");
        $("body").removeClass("over_hid");
    });

    /*------------------
		Navigation
	--------------------*/
    $(".mobile-menu").slicknav({
        prependTo: '#mobile-menu-wrap',
        allowParentLinks: true
    });

    $('.hero__categories__all').on('click', function() {
        $('.hero__categories ul').slideToggle(400);
    });

    /*-----------------------
		Price Range Slider
	------------------------ */
    var rangeSlider = $(".price-range")
      , minamount = $("#minamount")
      , maxamount = $("#maxamount")
      , minPrice = rangeSlider.data('min')
      , maxPrice = rangeSlider.data('max');
    rangeSlider.slider({
        range: true,
        min: minPrice,
        max: maxPrice,
        values: [minPrice, maxPrice],
        slide: function(event, ui) {
            minamount.val('$' + ui.values[0]);
            maxamount.val('$' + ui.values[1]);
        }
    });
    minamount.val('$' + rangeSlider.slider("values", 0));
    maxamount.val('$' + rangeSlider.slider("values", 1));

    /*--------------------------
        Select
    ----------------------------*/
    $("select").niceSelect();

    /*-------------------
		Quantity change
	--------------------- */
    var proQty = $('.pro-qty');
    proQty.prepend('<span class="dec qtybtn">-</span>');
    proQty.append('<span class="inc qtybtn">+</span>');
    var $buyButton = $('.buy-button.buy-button-ref');
    proQty.on('click', '.qtybtn', function() {
        var $button = $(this);
        var oldValue = $button.parent().find('input').val();
        var newVal = parseFloat(oldValue);
        if ($button.hasClass('inc')) {
            newVal++;
        } else if (newVal > 1 && $button.hasClass(`dec`)) {
            newVal--;
        }
        $button.parent().find('input').val(newVal);
        $buyButton.attr('href', `/checkout/cart/add?sku=${skuJson.skus[0].sku}&qty=${newVal}&seller=1&redirect=true&sc=1`);
    });

    // Change tabs
    if(!$('.nav.nav-tabs .nav-item').length) return null;
    $('.nav.nav-tabs .nav-item').map(function(index, element) {
        $(element).on('click', function(ev) {
            var $this = $(ev.currentTarget);
            var $navLink = $('.nav-link');
            $navLink.removeClass('active')
            if (!$this.hasClass('active')) {
                $this.addClass('active');
                $this.children().addClass('active');
                $(element).removeClass('active');
            }
        })
    })

    // Search bar function
    const $form = document.querySelector('.hero__search__form form');
    $form.addEventListener('submit', (ev) => {
      ev.preventDefault();
      const $this = ev.target;
      const searchTerm = $this.querySelector('input');
      window.location.href = `/${searchTerm}`;
    })
}
)(jQuery);
