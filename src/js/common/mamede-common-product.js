const Methods = {
  init() {
    Methods.mountShelf();
    Methods.removeSpace();
    Methods.setShareLinks();
  },
  mountShelf() {
    const settings = {
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      arrows: false,
      centerMode: true,
      centerPadding: '10',
    };
    $('.helperComplement').remove();
    $('.t-shelf.js--shelf ul').slick(settings);
  },
  removeSpace() {
    const $uselessParagraph = document.querySelector('.product__details__price + p');
    $uselessParagraph.remove();
  },
  setShareLinks() {
    const $facebook = $('.share a:nth-child(1)');
    const $twitter = $('.share a:nth-child(2)');
    const $instagram = $('.share a:nth-child(3)');

    $facebook.on('click', function(ev){
      ev.preventDefault();
      window.open(`https://www.facebook.com/sharer/sharer.php?${window.location.href}`,'_blank');
    });

    $twitter.on('click', function(ev){
      ev.preventDefault();
      window.open(`https://twitter.com/share?${window.location.href}`,'_blank');
    });

    $instagram.on('click', function(ev){
      ev.preventDefault();
      window.open(`https://www.instagram.com/mamedehaus`,'_blank');
    })
  }
};

document.addEventListener('DOMContentLoaded', Methods.init);