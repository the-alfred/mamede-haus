const Methods = {
  init() {
    Methods.search();
    Methods.slickShelf();
  },
  search() {
    const $form = document.querySelector('.hero__search__form form');
    $form.addEventListener('submit', (ev) => {
      ev.preventDefault();
      const $this = ev.target;
      const searchTerm = $this.querySelector('input').value;
      window.location.href = `/${searchTerm}`;
    })
  },
  slickShelf() {
    $('.helperComplement').remove();
    const $shelf = $('.js--shelf ul');
    const $shelf5 = $('.is--last .js--shelf ul');
    $shelf5.slick({
      slidesToShow: 5,
      arrows: false,
      autoplay: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 4,
          }
        }
      ]
    });
    $shelf.slick({
      slidesToShow: 1,
      arrows: false,
      autoplay: true,
    });
  }
};

document.addEventListener('DOMContentLoaded', Methods.init);