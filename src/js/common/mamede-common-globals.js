const Methods = {
  init() {
    Methods.slickShelf();
    Methods.newsletter();
    setTimeout(function() {
      Methods.isAuthenticated();
    }, 2500)
  },
  isAuthenticated() {
    return fetch('/no-cache/profileSystem/getProfile')
      .then((res) => res.json())
      .then((response) => {
        if(!response.IsUserDefined) return null;
          const $headerText = $('.header__top__right__auth a span');
          $headerText.text(response.FirstName);
      })
  },
  slickShelf() {
    $('.helperComplement').remove();
    const $shelf = $('.js-shelf ul');
    $shelf.slick({
      slidesToShow: 1
    })
  },
  _sendData(data) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/vnd.vtex.ds.v10+json');

    const settings = {
      mode: 'cors',
      cache: 'default',
      body: JSON.stringify({
        email: data,
      }),
      method: 'POST',
      headers,
    }

    try {
      fetch('/api/dataentities/NL/documents', settings).then((res) => res.json());
      alert('Email cadastrado com sucesso!');
    } catch(err) {
      alert('Erro ao cadastrar email. Por favor tente novamente!');
      throw new Error('Failed to send data to masterdata', err);
    }
  },
  newsletter() {
    const $form = document.querySelector('.footer__widget form');
    const $inputForm = $form.querySelector('input');
    $form.addEventListener('submit', function(ev) {
      ev.preventDefault();
      const $input = $inputForm.value;
      Methods._sendData($input);
      $input.value = '';
    });
  },
};

document.addEventListener('DOMContentLoaded', Methods.init);