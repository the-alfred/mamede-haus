const MiniCssExtractPlugin = require('mini-css-extract-plugin');

sassPlugins = [
  new MiniCssExtractPlugin({
    filename: "[name].css",
    chunkFilename: "[id].css"
}),
];

module.exports = sassPlugins;