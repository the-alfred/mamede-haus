const javascriptLoader = {
  test: /\.js$|.ts$/,
  exclude: /node_modules/,
  use: {
      loader: 'babel-loader',
      options: {
          presets: ['@babel/preset-env']
      }
  }
};
module.exports = javascriptLoader;