const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const sassLoader = {
  test: /\.scss$/,
  use: [
    'style-loader',
    MiniCssExtractPlugin.loader,
    {
      loader: "css-loader",
      options: {
        minimize: true,
        sourceMap: true
      }
    },
    {
      loader: "sass-loader"
    }
  ]
};


module.exports = sassLoader;