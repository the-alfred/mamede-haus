const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const javascriptLoader = require('./webpack/production/modules/loaders/javascript');

const storeName = 'mamede';

const config = {
  entry: {
    home: [
      path.resolve(__dirname, 'src') + `/js/common/${storeName}-common-home.js`,
      path.resolve(__dirname, 'src') + `/sass/common/${storeName}-common-home.scss`,
    ],
    category: [
      path.resolve(__dirname, 'src') + `/js/common/${storeName}-common-category.js`,
      path.resolve(__dirname, 'src') + `/sass/common/${storeName}-common-category.scss`,
    ],
    product: [
      path.resolve(__dirname, 'src') + `/js/common/${storeName}-common-product.js`,
      path.resolve(__dirname, 'src') + `/sass/common/${storeName}-common-product.scss`,
    ],
    search: [
      path.resolve(__dirname, 'src') + `/js/common/${storeName}-common-search.js`,
      path.resolve(__dirname, 'src') + `/sass/common/${storeName}-common-search.scss`,
    ],
    globals: [
      path.resolve(__dirname, 'src') + `/js/common/${storeName}-common-globals.js`,
      path.resolve(__dirname, 'src') + `/sass/common/${storeName}-common-globals.scss`,
    ],
    checkout: [
      path.resolve(__dirname, 'src') + `/js/common/${storeName}-common-checkout.js`,
      path.resolve(__dirname, 'src') + `/sass/common/${storeName}-common-checkout.scss`,
    ],
  },
  output: {
    filename: `arquivos/${storeName}-common-[name].js`,
    path: path.resolve(__dirname, 'dist'),
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename: `arquivos/${storeName}-common-[name].css`,
    }),
  ],
  module: {
    rules: [{
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              url: false,
              importLoaders: 1
            }
          },
          {
            loader: 'postcss-loader',
          },
          {
            loader: 'sass-loader'
          },
        ],
      },
      javascriptLoader
    ]
  },
  resolve: {
    extensions: ['.css', '.scss'],
  }
};

module.exports = config;